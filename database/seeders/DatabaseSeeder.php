<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Comment;
use App\Models\Publication;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        $this->call([UserSeeder::class]);
        $this->call([TypeSeeder::class]);
        Publication::factory(50)
            ->has(Comment::factory()->count(5))
            ->create();
        // $this->call([ProductoSeeder::class]);
        // $this->call([PublicationSeeder::class]);
        // $this->call([CommentSeeder::class]);
    }
}
