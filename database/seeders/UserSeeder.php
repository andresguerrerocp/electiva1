<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Andrés Guerrero Guerrero',
            'email' => 'andresguerrerocp@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'name' => 'Diana Marcela Zapata Escobar',
            'email' => 'dzapata89@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'name' => 'Juan Pablo Ospina Bedoya',
            'email' => 'jpospina@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'name' => 'Eddi Andrés García Cañas',
            'email' => 'eddi_gc07@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        Role::create(['name' => 'Autor']);
        Role::create(['name' => 'Bibliotecario']);
        Role::create(['name' => 'Administrador']);
        Role::create(['name' => 'Usuario']);

        $user = User::find(1);
        $user->assignRole('Administrador');

        $user = User::find(2);
        $user->assignRole('Usuario');

        $user = User::find(3);
        $user->assignRole('Bibliotecario');
        
        $user = User::find(4);
        $user->assignRole('Autor');
    }
}
