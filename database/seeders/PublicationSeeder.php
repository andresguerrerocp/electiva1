<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PublicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'nombre' => 'Lenovo S340',
            'categoria' => 'Computador Personal',
            'precio' => 2500000
        ]);
    }
}
