<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            ['type' => 'Articulo', 'user_id' => 1],
            ['type' => 'Libro', 'user_id' => 1],
            ['type' => 'Revista', 'user_id' => 1],
        ]);
    }
}
