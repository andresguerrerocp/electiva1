<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::insert('insert into products (nombre, categoria, precio) values (?, ?, ?)', 
        // ['Lenovo S340', 'Computador Personal', 2500000]);

        DB::table('products')->insert([
            'nombre' => 'Lenovo S340',
            'categoria' => 'Computador Personal',
            'precio' => 2500000
        ]);
    }
}
