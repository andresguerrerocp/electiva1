<?php

namespace App\Http\Livewire\Nav;

use Livewire\Component;
use App\Models\Publication;
use Illuminate\Support\Facades\Auth;

class Notification extends Component
{
    public $notifications = [];

    protected $listener = ['loadNotifications'];

    public function mount()
    {
        foreach(Auth::user()->unreadNotifications as $notification)
        {
            array_push($this->notifications, $notification->data['publication_id']);
        }
        $this->notifications = Publication::whereIn('id', $this->notifications)->latest()->get();
    }

    public function loadNotifications()
    {
        $this->dispatchBrowserEvent('countNotifications', ['count' => Auth::user()->unreadNotifications()->count()]);
        return true;
    }
    public function render()
    {
        return view('livewire.nav.notification');
    }
}
