<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class Create extends Component
{
    public $name;
    public $email;
    public $role;
    public $user;
    public $password;

    protected $listeners = ['setUser'];

    protected $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'role' => 'required'
    ];

    protected $messages = [
        'name.required' => 'Los nombres son obligatorios.',
        'email.required' => 'El correo es obligatorio.',
        'email.email' => 'El correo no tiene un formato válido.',
        'role.required' => 'Debe elegir un rol.'
    ];

    public function updatingName()
    {
        $this->name = strtolower($this->name);
        $this->name = ucwords($this->name);
    }

    public function setUser($user)
    {
        if($user == 'create')
        {
            $this->user = $user;
            $this->name = '';
            $this->email = '';
            $this->role = 1;
            $this->dispatchBrowserEvent('changeTitle', ['title' => 'Crear Usuario']);
            return true;
        }else{
            $this->dispatchBrowserEvent('changeTitle', ['title' => 'Editar Usuario']);
        }
        $this->user = User::find($user);
        $this->name = $this->user->name;
        $this->email = $this->user->email;
        $this->role = $this->user->roles()->first()->id;
    }

    public function save()
    {
        $this->validate();
        if($this->user !== 'create')
        {
            $this->user->name = $this->name;
            $this->user->email = $this->email;
            if(!empty($this->password))
            {
                $this->user->password = Hash::make($this->password);
            }
            $this->user->removeRole($this->user->roles()->first()->name);
            $this->user->assignRole(Role::findOrFail($this->role));
            // $this->user->save();
            $this->user->update();
            $this->dispatchBrowserEvent('alert', [
                'title' => 'Usuario editado!',
                'message' => 'Se han realizado los cambios correctamente.',
                'icon' => 'success',
                'cerrar' => true
            ]);
        }else{
            if(empty($this->password)){
                $this->dispatchBrowserEvent('alert', [
                    'title' => 'Error al crear',
                    'message' => 'Debe digitar una contraseña',
                    'icon' => 'error'
                ]);
                return true;
            }
            $user = User::create([
                'name' => $this->name,
                'email' => $this->email,
                'password' => Hash::make($this->password),
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ]);

            $user->assignRole(Role::findOrFail($this->role));

            $this->dispatchBrowserEvent('alert', [
                'title' => 'Usario creado!',
                'message' => 'Se ha creado el usuario correctamente.',
                'icon' => 'success',
                'cerrar' => true
            ]);
        }
        
    }
    
    public function render()
    {
        return view('livewire.user.create', [
            'roles' => Role::all()
        ]);
    }
}
