<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Index extends Component
{
    public $search;

    public function sendUser($user)
    {
        $this->emitTo('user.create', 'setUser', $user);
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
    }

    public function render()
    {
        if(!empty($this->search))
        {
            $users = User::where('name', 'LIKE', "%{$this->search}%")
                ->orWhere('email', 'LIKE', "%{$this->search}%")
                ->get();
        }else{
            $users = User::where('id', '<>', Auth::user()->id)->get();
        }
        return view('livewire.user.index', [
            'users' => $users
        ]);
    }
}
