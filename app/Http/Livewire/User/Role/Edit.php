<?php

namespace App\Http\Livewire\User\Role;

use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Edit extends Component
{
    public Role $role;
    public $permissions;

    public function mount()
    {
        $this->permissions = Permission::get();
    }

    public function assignPermission(Permission $permission)
    {
        if(!$this->role->hasPermissionTo($permission->name)){
            $this->role->givePermissionTo($permission->name);
        }else{
            $this->role->revokePermissionTo($permission->name);
        }
    }

    public function render()
    {
        return view('livewire.user.role.edit');
    }
}
