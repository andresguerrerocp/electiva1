<?php

namespace App\Http\Livewire\User\Role;

use Livewire\Component;
use Spatie\Permission\Models\Role;

class Index extends Component
{
    public $roles;
    public $search;

    public function render()
    {
        $this->roles = Role::when(!empty($this->search), function($q) {
            $q->where('name', 'LIKE', "%{$this->search}%");
        })->get();
        return view('livewire.user.role.index');
    }
}
