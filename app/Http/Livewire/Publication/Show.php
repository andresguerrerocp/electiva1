<?php

namespace App\Http\Livewire\Publication;

use App\Models\Publication;
use Livewire\Component;

class Show extends Component
{
    public $publication;

    protected $listeners = ['comments'];

    public function sendId()
    {
        $this->emitTo('publication.comment', 'setPublicationId', $this->publication);
    }

    public function comments()
    {
        return true;
    }

    public function render()
    {
        $this->comments();
        return view('livewire.publication.show');
    }
}
