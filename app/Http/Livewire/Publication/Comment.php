<?php

namespace App\Http\Livewire\Publication;

use App\Models\Comment as ModelsComment;
use App\Models\Publication;
use Livewire\Component;

class Comment extends Component
{
    public $comment;
    public $publication;

    protected $listeners = ['setPublicationId'];

    protected $rules = [
        'comment' => 'required'
    ];

    protected $messages = [
        'comment.required' => 'Debe dejar un texto de comentario.'
    ];

    public function setPublicationId(Publication $publication)
    {
        $this->publication = $publication;
    }

    public function addComment()
    {
        $this->validate();

        ModelsComment::create([
            'user_id' => auth()->user()->id,
            'publication_id' => $this->publication->id,
            'comment' => $this->comment
        ]);

        $this->emitTo('publication.show', 'comments');

        $this->dispatchBrowserEvent('alert', ['title' => $this->publication->title]);

        $this->comment = '';
    }

    public function render()
    {
        return view('livewire.publication.comment');
    }
}
