<?php

namespace App\Http\Livewire\Publication;

use Livewire\Component;
use App\Models\Publication;
use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $queryString = [
        'search' => ['except' => '']
    ];
    public $search='';

    protected $listeners = ['publications'];

    public function publications()
    {
        return true;
    }

    public function openModal($title, $id=null)
    {
        $this->emitTo('publication.create', 'openModal', $title, $id);
    }
    
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function delete($id)
    {
        $publication = Publication::find($id);
        if(Auth::user()->can('Eliminar Publicacion') && ($publication->user_id == Auth::user()->id))
        {
            $publication->delete();
        }else{
            $this->dispatchBrowserEvent('alert', [
                'title' => 'Error',
                'html' => 'No tiene permisos para eliminar esta publicación',
                'icon' => 'error'
            ]);
        }
    }

    public function render()
    {
        $publications = Publication::when(!empty($this->search), function($q){
            $q->where('title', 'REGEXP', $this->search)
                ->orWhere('description', 'REGEXP', $this->search)
                ->orWhereHas('user', function($query){
                    return $query->where('name', 'REGEXP', $this->search);
                });
        })->when(auth()->user()->hasRole('Usuario'), function($q){
            $q->where('state', 'Disponible');
        })->latest()->paginate(5);
        
        return view('livewire.publication.index', compact('publications'));
    }
}
