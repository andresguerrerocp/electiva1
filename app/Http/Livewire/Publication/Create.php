<?php

namespace App\Http\Livewire\Publication;

use App\Models\Publication;
use App\Models\Type;
use App\Providers\Published;
use Livewire\Component;

class Create extends Component
{
    public $title;
    public $description;
    public $types = [];
    public $type=1;
    public $typeForm;
    public $publication=null;

    public $action='<i class="fas fa-fw fa-plus"></i> Crear';
    public $a='crear';

    public $listeners = ['openModal'];

    protected $rules = [
        'title' => 'required',
        'description' => 'required|min:25'
    ];

    protected $messages = [
        'title.required' => 'Debe escribir un título.',
        'description.required' => 'Debe escribir una descripción',
        'description.min' => 'La descripción debe ser mínimo de 25 caracteres.'
    ];

    public function mount()
    {
        $this->types = Type::all();
    }

    public function openModal($title, $id)
    {
        if($id)
        {
            $this->publication = Publication::find($id);
            $this->title = $this->publication->title;
            $this->description = $this->publication->description;
            $this->type = $this->publication->type_id;
        }

        if($title == "Editar Publicación")
        {
            $this->action = '<i class="fas fa-fw fa-save"></i> Guardar';
            $this->a = 'edit';
        }else{
            $this->action = '<i class="fas fa-fw fa-plus"></i> Crear';
            $this->a = 'create';
            
            $this->title = '';
            $this->description = '';
            $this->type = 1;
            $this->publication=null;
        }
        $this->dispatchBrowserEvent('openModal', ['title' => $title]);
    }

    public function create()
    {
        $this->validate();
        $publication = Publication::create([
            'title' => $this->title,
            'description' => $this->description,
            'user_id' => auth()->user()->id,
            'state' => 'Publicado',
            'type_id' => $this->type
        ]);

        event(new Published($publication));

        $this->dispatchBrowserEvent('alert', [
            'title' => 'Publicación Creada',
            'msg' => "Se ha creado correctamente la publicación <strong>{$this->title}</strong>",
            'icon' => 'success'
        ]);

        $this->emitTo('publication.index', 'publications');
        $this->emitTo('nav.notification', 'loadNotifications');

        $this->title = '';
        $this->description = '';
        $this->type = '';
    }

    public function edit()
    {
        $this->publication->title = $this->title;
        $this->publication->description = $this->description;
        $this->publication->type_id = $this->type;
        $this->publication->save();
        
        $this->dispatchBrowserEvent('alert', [
            'title' => 'Publicación Editada',
            'msg' => "Se ha editado correctamente la publicación <strong>{$this->title}</strong>",
            'icon' => 'success'
        ]);
        $this->emitTo('publication.index', 'publications');
    }

    public function render()
    {
        return view('livewire.publication.create');
    }
}
