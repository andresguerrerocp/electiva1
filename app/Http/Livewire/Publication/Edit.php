<?php

namespace App\Http\Livewire\Publication;

use Livewire\Component;

class Edit extends Component
{
    public function render()
    {
        return view('livewire.publication.edit');
    }
}
