<?php

namespace App\Http\Controllers;

use App\Models\Publication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index()
    {
        $notificationsArray = [];

        foreach(Auth::user()->unreadNotifications as $notification)
        {
            array_push($notificationsArray, $notification->data['publication_id']);
        }
        
        $notifications = Publication::whereIn('id', $notificationsArray)->latest()->get();

        return view('user.notification', compact('notifications'));
    }
}
