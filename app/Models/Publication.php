<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publication extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    /**
     * Get the user associated with the Publication
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function getBadgeAttribute()
    {
        $badge = 'badge ';

        if($this->state=='Disponible')
        {
            $badge .= 'bg-success';
        }else if($this->state=='Publicado')
        {
            $badge .= 'bg-primary';
        }else if($this->state=='Revisado')
        {
            $badge .= 'bg-warning text-white';
        }else if($this->state=='Préstamo')
        {
            $badge .= 'bg-danger';
        }
        else if($this->state=='Aprobado')
        {
            $badge .= 'bg-info';
        }

        return $badge;
    }

    public function getTitleHTMLAttribute()
    {
        return str_replace(array('\'', '"'), '', $this->title);
    }
}
