<?php

namespace App\Providers;

use App\Notifications\PublicationState;
use App\Providers\Published;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNotificationPublished
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Published  $event
     * @return void
     */
    public function handle(Published $event)
    {
        $event->publication;

        auth()->user()->notify(new PublicationState($event->publication));
    }
}
