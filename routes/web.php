<?php

use App\Http\Controllers\NotificationController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('productos', ProductController::class);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/publicacion', [PublicationController::class, 'index'])->name('publication.index');

// Route::group(['middleware' => ['role:Autor']], function () {
Route::group(['middleware' => ['auth:sanctum']], function () {
    //rutas accesibles solo para admin
    Route::get('/', function () {
        return view('home');
    })->name('home');

    Route::resource('publicacion', PublicationController::class)->parameters(['publicacion' => 'publication']);

    Route::group(['middleware' => ['role:Administrador|Bibliotecario']], function () {
        Route::get('notificaciones', [NotificationController::class, 'index'])->name('notification.index');
    });
    
    Route::group(['middleware' => ['role:Administrador']], function () {
        Route::resource('usuarios', UserController::class)->parameters([
            'usuarios' => 'user'
        ]);
        Route::get('roles', [RoleController::class, 'index'])->name('roles.index');
        Route::get('roles/editar/{role}', [RoleController::class, 'edit'])->name('roles.edit');
    });
});
