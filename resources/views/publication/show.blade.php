@extends('layout.master')

@section('contenido')
    @livewire('publication.show', ['publication' => $publication])

    <div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalTitle" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <p class="modal-title h5" id="commentModalTitle">
                        Comentar
                    </p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @livewire('publication.comment')
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        var modalComment = new bootstrap.Modal(document.getElementById('commentModal'), {
            keyboard: false
        });
    </script>
@endpush