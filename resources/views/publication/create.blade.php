@extends('layout.master')

@section('contenido')
<div class="card mt-3">
    <div class="card-header bg-dark text-white">
        <h5 class="card-title">
            Crear Publicación <i class="fas fa-book-open"></i>
        </h5>
    </div>
    <div class="card-body">
        @livewire('publication.create', ['typeForm' => 'card'])
    </div>
</div>
@endsection