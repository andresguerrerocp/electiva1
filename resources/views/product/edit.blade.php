@extends('layout.master')

@section('contenido')
    <div class="card mt-5">
        <div class="card-header">
            Crear Producto
        </div>
        <form action="{{ route('productos.update') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col form-group">
                        <label for="nombre">Nombre:</label>
                        <input class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre') }}">
                    </div>
                    <div class="col form-group">
                        <label for="categoria">Categoria:</label>
                        <input class="form-control" type="text" name="categoria" id="categoria" value="{{ old('categoria') }}">
                    </div>
                    <div class="col form-group">
                        <label for="precio">Precio:</label>
                        <input class="form-control" type="number" name="precio" id="precio" value="{{ old('precio') }}">
                    </div>
                    <div class="col align-self-end">
                        <button class="btn btn-primary">
                            Guardar
                        </button>
                    </div>
                </div>
                <div class="row">
                    @if ($errors->any())
                        <div class="alert alert-danger mt-4" role="alert">
                            @foreach ($errors->all() as $error)
                                <p>
                                    {{ $error }}
                                </p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
@endsection