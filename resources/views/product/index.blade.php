@extends('layout.master')

@section('contenido')
    <div class="card mt-5">
        <div class="card-header">
            Lista Productos
        </div>
        <div class="card-body">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Categoría</th>
                        <th>Precio</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->nombre }}</td>
                            <td>{{ $product->categoria }}</td>
                            <td>${{ number_format($product->precio, 2) }}</td>
                            <td>
                                <a class="btn btn-sm btn-success" href="{{ route('productos.show', $product) }}">Editar</a>
                                <form class="d-inline" action="{{ route('productos.destroy', $product) }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection