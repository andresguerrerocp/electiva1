@extends('layout.master')

@section('contenido')
    @livewire('user.index')

    <div class="modal fade" id="editUser" tabindex="-1" aria-labelledby="editUserTitle" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title h5" id="editUserTitle">
                        <i class="fas fa-users-cog"></i>
                        Editar Usuario
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @livewire('user.create')
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        var modalEditUser = new bootstrap.Modal(document.getElementById('editUser'), {
            keyboard: false
        });
    </script>
@endpush