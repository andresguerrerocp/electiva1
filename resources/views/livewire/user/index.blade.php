<div>
    <h1 class="app-page-title">
        <i class="fas fa-users"></i>
        Usuarios
    </h1>
    <div class="app-card app-card-orders-table">
        <div class="app-card-body">
            <div class="row mb-2 p-3">
                <div class="col-3 form-group">
                    <label for="search">
                        <i class="fas fa-search"></i>
                        Buscar...
                    </label>
                    <input wire:model='search' type="search" class="form-control">
                </div>
                <div class="col form-group align-self-end">
                    <button wire:click="sendUser('create')" type="button" class="btn app-btn-primary" data-bs-toggle="modal" data-bs-target="#editUser">
                        <i class="fas fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="table-responsive p-3">
                <table class="table app-table-hover">
                    <thead>
                        <tr>
                            <th class="cell">Id</th>
                            <th class="cell">Nombres</th>
                            <th class="cell">Email</th>
                            <th class="cell">Tipo</th>
                            <th class="cell">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @foreach ($user->roles as $role)
                                        {{ $role->name }}
                                        @if (!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    <button wire:click="sendUser({{ $user->id }})" class="btn btn-sm app-btn-primary" data-bs-toggle="modal" data-bs-target="#editUser">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="btn btn-sm btn-danger text-white" onclick="delUser('{{ $user->id }}', '{{ $user->name }}')">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        const delUser = (user_id, user)=>{
            Swal.fire({
                title: '¿Estás seguro?',
                html: `Está acción eliminará el usuario <strong>${user}</strong>!`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrar!',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                    @this.delete(user_id)
                    Swal.fire({
                        title: 'Eliminada!',
                        html: `Has eliminado el usuario <strong>${user}.</strong>`,
                        icon: 'success',
                    });
                }
            });
        };
    </script>
@endpush