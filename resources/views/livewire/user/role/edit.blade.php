<div>
    <div class="app-card app-card-progress-list h-100 shadow-sm">
        <div class="app-card-header p-3">
            <div class="row justify-content-between align-items-center">
                <div class="col-auto">
                    <h4 class="app-card-title">{{ $role->id }}-{{ $role->name }} ({{ $role->permissions->count() }} permisos asociados)</h4>
                </div><!--//col-->
                <div class="col-auto">
                    <div class="card-header-action">
                        <a href="{{ route('roles.index') }}">Volver a Roles</a>
                    </div><!--//card-header-actions-->
                </div><!--//col-->
            </div><!--//row-->
        </div><!--//app-card-header-->
        <div class="app-card-body">
            @foreach ($permissions as $permission)
                <div class="item p-3">
                    <div class="row align-items-center">
                        <div class="col">
                            <div class="form-check">
                                <input wire:click='assignPermission({{ $permission->id }})' class="form-check-input" type="checkbox" value="" id="{{ $permission->guard_name }}_{{ $permission->name }}" {{ in_array($permission->name, $role->permissions->pluck('name')->toArray()) ? 'checked' : '' }}>
                                <label class="form-check-label" for="{{ $permission->guard_name }}_{{ $permission->name }}">
                                    {{ $permission->name }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div><!--//item-->
            @endforeach
        </div><!--//app-card-body-->
    </div>
</div>
