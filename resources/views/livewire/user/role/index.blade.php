<div>
    <h1 class="app-page-title">
        <i class="fas fa-users"></i>
        Roles y Permisos
    </h1>
    <div class="app-card app-card-orders-table">
        <div class="app-card-body">
            <div class="row mb-2 p-3">
                <div class="col-3 form-group">
                    <label for="search">
                        <i class="fas fa-search"></i>
                        Buscar...
                    </label>
                    <input wire:model='search' type="search" class="form-control">
                </div>
                <div class="col form-group align-self-end">
                    <button wire:click="sendUser('create')" type="button" class="btn app-btn-primary" data-bs-toggle="modal" data-bs-target="#editUser">
                        <i class="fas fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="table-responsive p-3">
                <table class="table app-table-hover">
                    <thead>
                        <tr>
                            <th class="cell">Id</th>
                            <th class="cell">Nombre</th>
                            <th class="cell">Creado</th>
                            <th class="cell">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $role)
                            <tr>
                                <td>{{ $role->id }}</td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->created_at->diffForHumans() }}</td>
                                <td>
                                    <a href="{{ route('roles.edit', $role) }}" class="btn btn-sm app-btn-primary">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <button class="btn btn-sm btn-danger text-white">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
