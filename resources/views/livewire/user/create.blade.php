<div>
    <form wire:submit.prevent="save" method="post">
        <div class="row">
            <div class="col form-group">
                <label for="name">Nombre</label>
                <input wire:model='name' type="text" class="form-control">
                @error('name')
                    <div class="text-danger">
                            {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col form-group">
                <label for="email">Email</label>
                <input wire:model='email' type="email" class="form-control">
            </div>
            @error('email')
                <div class="text-danger">
                        {{ $message }}
                </div>
            @enderror
        </div>
        <div class="row">
            <div class="col form-group">
                <label for="password">Contraseña</label>
                <input wire:model="password" type="password" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col form-group">
                <label for="role">Rol</label>
                <select wire:model='role' class="form-select">
                    @foreach ($roles as $role)
                        <option value="{{ $role->id }}">
                            {{ $role->name }}
                        </option>
                    @endforeach
                </select>
                @error('role')
                    <div class="text-danger">
                            {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="row mt-3">
            <div class="col form-group align-self-end">
                <button class="btn btn-primary">
                    Guardar
                    <i class="fas fa-save"></i>
                </button>
            </div>
        </div>
    </form>
</div>

@push('js')
    <script>
        window.addEventListener('changeTitle', event=>{
            let title = document.getElementById('editUserTitle');
            title.innerHTML = '<i class="fas fa-users-cog"></i> ' + event.detail.title;
        });

        window.addEventListener('alert', event=>{
            Swal.fire(
                event.detail.title,
                event.detail.message,
                event.detail.icon
            );

            if(event.detail.cerrar){
                modalEditUser.hide();
            }
        });

    </script>
@endpush