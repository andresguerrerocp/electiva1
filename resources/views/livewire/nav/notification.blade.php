<div class="dropdown-menu p-0" aria-labelledby="notifications-dropdown-toggle">
    <div class="dropdown-menu-header p-3">
        <h5 class="dropdown-menu-title mb-0">Notificaciones</h5>
    </div><!--//dropdown-menu-title-->
    <div class="dropdown-menu-content">
        @foreach ($notifications as $not)    
            <div class="item p-3">
                <div class="row gx-2 justify-content-between align-items-center">
                    <div class="col-auto">
                        <div class="app-icon-holder">
                            <i class="fas fa-newspaper"></i>
                        </div>
                    </div><!--//col-->
                    <div class="col">
                        <div class="info"> 
                            <div class="desc">{{ $not->title }}</div>
                            <div class="meta">{{ $not->created_at->diffForHumans() }}</div>
                        </div>
                    </div><!--//col-->
                </div><!--//row-->
                <a class="link-mask" href="notifications.html"></a>
            </div><!--//item-->
        @endforeach
        <div class="item p-3">
                <div class="row gx-2 justify-content-between align-items-center">
                    <div class="col-auto">
                        <div class="app-icon-holder icon-holder-mono">
                            <i class="fas fa-book"></i>
                        </div>
                    </div><!--//col-->
                    <div class="col">
                        <div class="info"> 
                            <div class="desc">Your report is ready. Proin venenatis interdum est.</div>
                            <div class="meta"> 3 days ago</div>
                        </div>
                    </div><!--//col-->
                </div><!--//row-->
                <a class="link-mask" href="notifications.html"></a>
        </div><!--//item-->
    </div><!--//dropdown-menu-content-->
    
    <div class="dropdown-menu-footer p-2 text-center">
        <a href="{{ route('notification.index') }}">Ver todas</a>
    </div>    
</div><!--//dropdown-menu-->

@push('js')
    <script>
        window.addEventListener('countNotifications', event=>{
            console.log(event.detail)
            document.getElementById('countNotifications').innerText = event.detail.count
        })
    </script>
@endpush