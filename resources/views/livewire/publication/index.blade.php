<div>
    <h1 class="app-page-title">
        <i class="fas fa fa-book"></i>
        Publicaciones
    </h1>
    <div class="app-card app-card-orders-table">
        <div class="app-card-body">
            <div class="row mb-2 p-3">
                <div class="col-3 form-group">
                    <label class="form-label" for="search">
                        <i class="fas fa-fw fa-search"></i>
                        <strong>Buscar...</strong>
                    </label>
                    <input wire:model="search" type="search" class="form-control">
                </div>
                @can('Publicar')    
                    <div class="col-2 form-group align-self-end">
                        <button wire:click="openModal('Nueva Publicación')" type="button" class="btn app-btn-primary">
                            <i class="fas fa-plus"></i> Crear
                        </button>
                    </div>
                @endcan
            </div>
            <div class="table-responsive p-3">
                <table class="table app-table-hover">
                    <thead>
                        <tr>
                            <th class="cell">Titulo</th>
                            <th class="cell">Descripción</th>
                            <th class="cell">Autor</th>
                            <th class="cell">Tipo</th>
                            <th class="cell">Estado</th>
                            <th class="cell">Publicado</th>
                            <th class="cell text-center" style="white-space:nowrap;">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($publications as $publication)
                            <tr>
                                <td class="align-middle">{{ $publication->title }}</td>
                                <td class="align-middle">{{ $publication->description }}</td>
                                <td class="align-middle">{{ $publication->user->name }}</td>
                                <td class="align-middle">{{ $publication->type->type }}</td>
                                <td class="align-middle"> 
                                    <span class="{{ $publication->badge }}">
                                    {{ $publication->state }}
                                    </span>
                                </td>
                                <td class="align-middle" style="white-space:nowrap;">
                                    {{-- {{ $publication->created_at->format('d/m/Y'); }} --}}
                                    {{ $publication->created_at->diffForHumans(); }}
                                </td>
                                @auth    
                                    <td class="align-middle" style="white-space:nowrap;">
                                        <a class="btn btn-sm btn-info text-white" href="{{ route('publicacion.show', $publication) }}">
                                            <i class="fas fa-info-circle"></i>
                                        </a>

                                        @if (Auth::user()->can('Editar Publicacion') && ($publication->user_id == Auth::user()->id)) 
                                            <button wire:click="openModal('Editar Publicación', {{ $publication->id }})" class="btn btn-sm app-btn-primary">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        @endif

                                        @if (Auth::user()->can('Eliminar Publicacion') && ($publication->user_id == Auth::user()->id))
                                            <button class="btn btn-sm btn-danger text-white" onclick="delPublication({{ $publication->id }}, '{!! $publication->titleHTML !!}', '{{ $publication->state }}')">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                    </td>
                                @endauth
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $publications->links() }}
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        const delPublication = (publication_id, publication_title, publication_state)=>{
            Swal.fire({
                title: '¿Estás seguro?',
                html: `Está acción eliminará la publicación <strong>${publication_title}</strong> seleccionada!`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, borrar!',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                    if(publication_state == 'Préstamo') {
                        Swal.fire({
                            title: 'Upps!',
                            html: `No puedes eliminar la publicación <strong>${publication_title}</strong> ya que se encuentra en préstamo.`,
                            icon: 'error',
                        });
                    }else{
                        @this.delete(publication_id)
                        Swal.fire({
                            title: 'Eliminada!',
                            html: `Has eliminado la publicación <strong>${publication_title}.</strong>`,
                            icon: 'success',
                        });
                    }
                }
            });
        };

        window.addEventListener('alert', event=>{
            Swal.fire({
                title: event.detail.title,
                html: event.detail.html,
                icon: event.detail.icon
            });
        });
    </script>
@endpush