<div>
    <form wire:submit.prevent='addComment' method="POST">
        <div class="row">
            <div class="col form-group">
                <label for="comment">
                    <strong>
                        {{ $publication->title ?? '' }}
                    </strong>
                </label>
                <textarea wire:model='comment' class="form-control"></textarea>
                @error('comment')
                    <div class="text-danger">
                            {{ $message }}
                    </div>
                 {{-- <span class="error">{{ $message }}</span> --}}
                @enderror
            </div>
        </div>
        <div class="row mt-2">
            <div class="col form-group">
                <button class="btn btn-secondary">
                    <i class="fas fa-fw fa-plus"></i> Agregar
                </button>
            </div>
        </div>
    </form>
</div>

@push('js')
    <script>
        window.addEventListener('alert', event => {
            Swal.fire(
                'Comentario guardado',
                'Se ha comentado la publicación ' + event.detail.title,
                'success'
            );
            modalComment.hide();
        });
    </script>
@endpush