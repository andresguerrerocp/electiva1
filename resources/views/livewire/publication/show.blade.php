<div>
    <div class="app-card app-card-basic d-flex flex-column align-items-start shadow-sm">
        <div class="app-card-header p-3 border-bottom-0">
            <div class="row align-items-center gx-3">
                <div class="col-auto">
                    <div class="app-icon-holder">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-journal-richtext" viewBox="0 0 16 16">
                            <path d="M7.5 3.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0zm-.861 1.542 1.33.886 1.854-1.855a.25.25 0 0 1 .289-.047L11 4.75V7a.5.5 0 0 1-.5.5h-5A.5.5 0 0 1 5 7v-.5s1.54-1.274 1.639-1.208zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
                            <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                            <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                        </svg>
                    </div><!--//icon-holder-->
                    
                </div><!--//col-->
                <div class="col-auto">
                    <h4 class="app-card-title">
                        {{ $publication->title }}
                    </h4>
                    <span class="{{ $publication->badge }} rounded-pill">{{ $publication->state }}</span>
                </div><!--//col-->
            </div><!--//row-->
        </div>
        <div class="app-card-body px-4">
            <p class="fs-6">
                {{ $publication->description }}
            </p>
        </div>
        <div class="app-card-footer p-4 mt-auto">
            <h6>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-lines-fill" viewBox="0 0 16 16">
                    <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z"/>
                </svg>
                {{ $publication->user->name }}
            </h6>
        </div>
    </div>
    {{-- comments --}}
    <div class="app-card app-card-stats-table h-100 shadow-sm mt-3">
        <div class="app-card-header p-3">
            <div class="row justify-content-start align-items-center">
                <div class="col-auto">
                    <div class="app-icon-holder icon-holder-mono">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat-left-text" viewBox="0 0 16 16">
                            <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H4.414A2 2 0 0 0 3 11.586l-2 2V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12.793a.5.5 0 0 0 .854.353l2.853-2.853A1 1 0 0 1 4.414 12H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                            <path d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                    </div>
                </div>
                <div class="col-auto">
                    <h4 class="app-card-title">
                        Comentarios
                    </h4>
                </div>
                <div class="col-auto">
                    @can('Comentar')    
                        <button wire:click="sendId({{ $publication->id }})" type="button" class="btn btn-sm app-btn-secondary" data-bs-toggle="modal" data-bs-target="#commentModal">
                            <i class="fas fa-plus"></i>
                        </button>
                    @endcan
                </div>
            </div>
            
        </div>
        <ul class="list-group">
            @forelse ($publication->comments as $comment)
                <li class="list-group-item d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        <div class="fw-bold">
                            {{ $comment->user->name }}
                        </div>
                        {{ $comment->comment }}
                    </div>
                    <span class="badge bg-secondary rounded-pill">{{ $comment->created_at->diffForHumans() }}</span>
                </li>
            @empty
                <li class="list-group-item d-flex justify-content-between align-items-start">
                    No existen comentarios
                </li>
            @endforelse
        </ul>
    </div>
</div>