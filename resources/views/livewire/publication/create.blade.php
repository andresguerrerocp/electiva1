<div>
    <form wire:submit.prevent="{{ $a }}">
        <div class="row">
            <div class="col-3 col-md-6 col-sm-6 form-group">
                <label for="title">Titulo</label>
                <input wire:model='title' type="text" class="form-control">
                @error('title')
                    <div class="text-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="col-2 col-md-6 col-sm-6 form-group">
                <label for="type">Tipo</label>
                <select wire:model="type" id="type" class="form-select">
                    @foreach ($types as $t)
                        <option value="{{ $t->id }}">{{ $t->type }}</option>
                    @endforeach
                </select>
                @error('type')
                    <div class="text-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col form-group">
                <label for="description">Descripción</label>
                <textarea wire:model='description' type="text" class="form-control h-20" rows="20"></textarea>
            </div>
            @error('description')
                <div class="text-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="row mt-2">
            <div class="col form-group">
                <button class="btn btn-secondary" id="btnActionPublication">
                    {!! $action !!}
                </button>
            </div>
        </div>
    </form>
</div>
@push('js')
    <script>
        document.addEventListener('DOMContentLoaded', ()=>{
            
        });

        window.addEventListener('openModal', event => {
            if(event.detail.title == "Editar Publicación")
            {
                document.getElementById('createPublicationTitle').innerHTML = '<i class="fas fa-book"></i> ' + event.detail.title;
                // document.getElementById('btnActionPublication').innerHTML = '<i class="fas fa-fw fa-save"></i> Guardar';
                modalCreate.show();
            }else{
                document.getElementById('createPublicationTitle').innerHTML = '<i class="fas fa-book"></i> ' + event.detail.title;
                // document.getElementById('btnActionPublication').innerHTML = '<i class="fas fa-fw fa-plus"></i> Crear';
                modalCreate.show();
            }
        });

        window.addEventListener('alert', event => {
            Swal.fire({
                title: event.detail.title,
                html: event.detail.msg,
                icon: event.detail.icon
            });
            if(modalCreate)
            {
                modalCreate.hide()
            }
        });
    </script>
@endpush