<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif
        <h1 class="h3 mb-3 fw-normal">Iniciar sesión</h1>
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-floating">
                <x-jet-input id="email" type="email" name="email" :value="old('email')" required autofocus />
                <x-jet-label for="email" value="{{ __('Email') }}" />
            </div>

            {{-- <div class="mt-4"> --}}
            <div class="form-floating">
                <x-jet-input id="password" type="password" name="password" required autocomplete="current-password" />
                <x-jet-label for="password" value="{{ __('Password') }}" />
            </div>
            
            <div class="flex items-center justify-end mt-4">
                <x-jet-button class="ml-4">
                    {{ __('Log in') }}
                </x-jet-button>
                @if (Route::has('password.request'))
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                    {{ __('Forgot your password?') }}
                </a>
                @endif
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember Me') }}</span>
                </label>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
